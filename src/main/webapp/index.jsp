<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login</title>
    <style type="text/css">
        .paddingBtm {
            padding-bottom: 12px;
        }
    </style>
</head>
<body>
<center>
    <h2>Login</h2>
    <form method="post" action="loginServlet">
        <div class="paddingBtm">
            <span id="user">Username: </span>
            <input type="text" name="username"/>
        </div>
        <div class="paddingBtm">
            <span id="pass">Password: </span>
            <input type="password" name="password"/>
        </div>
        <input type="submit" value="Login"/>
    </form>
</center>
</body>
</html>