package com.jcg.servlet;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/loginServlet")
public class Login extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html");
        String param1 = req.getParameter("username");
        String param2 = req.getParameter("password");

        if (param1 == null || param2 == null) {
            PrintWriter out = resp.getWriter();
            out.write("<p style='color: red; font-size: larger;'>You can't login without write anything!</p>");

        } else if ("".equals(param1) || "".equals(param2)) {
            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
            out.write("<p style='color: red; font-size: larger;'>Maybe you didn't write your name or password!</p>");
        } else {
            System.out.println("Username?= " + param1 + ", Password?= " + param2);
            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
            if ((param1.equalsIgnoreCase("admin")) && (param2.equals("admin"))) {
                out.write("<h2>Hi " + param1 + "</h2>");
                out.write("<p style='color: green; font-size: large;'>Congratulations! <span style='text-transform: capitalize;'>" + param1 + "</span>, You are an authorised login!</p>");
            } else {
                out.write("<p style='color: red; font-size: larger;'>You are not an authorised user! Please check with administrator!</p>");
            }
            out.write("</div></body></html>");
            out.close();
        }
    }
}